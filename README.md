# What does this do?
- This program allows minecraft server devs to install paper or spigot to a directory that they provide. 
# Installation
## Windows
With Windows you can install using the installer provided on the releases page. First, you need to install java, though. I like using the packages [here](https://adoptopenjdk.net/).
## MacOS
### With Brew
1. Run `brew install go`
2. Run `go get github.com/Henry0w/minecraft-server-installer/cmd/mcinstall`
3. This should allow you to run `mcinstall` in the terminal. If not, contact me for more troubleshooting.
### Without Brew
1. Install go [here](https://golang.com/dl/)
2. Run `go get github.com/Henry0w/minecraft-server-installer/cmd/mcinstall`
3. This should allow you to run `mcinstall` in the terminal. If not, contact me for more troubleshooting.
## Linux
1. Install go, git, and java using your package manager:  
`Arch:`  
`sudo pacman -S git jre8-openjdk go`  
`Debian/Ubuntu:`  
`sudo apt install git openjdk8-jre go`  
`Redhat/Fedora:`  
`sudo yum install java-1.8.0-openjdk git golang`
2. Run `go get github.com/Henry0w/minecraft-server-installer/cmd/mcinstall` in your terminal
3. You should be able to run `mcinstall` at the command line. Otherwise, run this command, then retry running `mcinstall`:
    + `PATH=+:$HOME/go/bin`
    + If this doesn't work, make an issue on the github page.

# Usage
`mcinstall [COMMAND] -o <server folder> -s <server software> -v <version>`  
**Command:**  
Install or update a minecraft server.  
Ex.:  
`mcinstall update`: Will update the minecraft server  
NOTE:
If you installed paper make sure you specify paper with -s, as it will default to Spigot.  
**Server Folder:**  
This is the folder that you want your server to be in. It will contain the plugins, worlds, and all the other goodies inside your server. If your folder includes whitespace, put quotes around the path.  
Default: N/A  
Windows Ex.:  
`-o C:\Users\henry0w\server-folder\ `  
Linux Ex.:  
`-o /home/henry0w/server-folder/`  

**Server Software:**  
We currently only support Spigot or Paper, but we have plans to add support for Forge, Fabric, Sponge, and Glowstone.  
Default: Spigot  
Ex.:  
`-s paper`

**Version:**  
With paper, we only support 1.15.2, 1.14.4, 1.12.2, and 1.8.8/1.8.9. Spigot supports all versions listed [here](https://www.spigotmc.org/wiki/buildtools/)  
Default: 1.15.2  
Ex.:  
`-v 1.14.4` 
