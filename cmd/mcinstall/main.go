package main

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/hashicorp/go-version"
	"github.com/henry0w/mcinstall/pkg/installer"
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"os/user"
	"strings"
	"time"
)

/*var usage = `	mcinstall {SERVER FOLDER} {SERVER SOFTWARE} {VERSION}

{SERVER FOLDER} -- Directory where server should be stored
{SERVER SOFTWARE} -- spigot/paper
{VERSION} -- Paper supports 1.15.2, 1.14.4, 1.12.2, and 1.8.8/1.8.9, spigot support all versions at https://www.spigotmc.org/wiki/buildtools/`*/
var spinChars = `|/-\`

type Spinner struct {
	message string
	i       int
}

func NewSpinner(message string) *Spinner {
	return &Spinner{message: message}
}

func (s *Spinner) Tick() {
	fmt.Printf("%s %c \r", s.message, spinChars[s.i])
	s.i = (s.i + 1) % len(spinChars)
}

func isTTY() bool {
	fi, err := os.Stdout.Stat()
	if err != nil {
		return false
	}
	return fi.Mode()&os.ModeCharDevice != 0
}

func main() {
	if os.Getenv("MCINSTALLER_FIRST_TIME") != "FALSE" {
		reader := bufio.NewReader(os.Stdin)
		fmt.Println("Hello! Thank's for using my program, a few disclaimers:")
		fmt.Println("You have a right to this program's source code under the GNU GPLv3 https://github.com/Henry0w/mcinstall")
		fmt.Printf("In order to use this program you must agree to mojang's EULA at https://account.mojang.com/documents/minecraft_eula [y/n]")
		text, err := reader.ReadString('\n')
		if !strings.Contains(text, "y") && !strings.Contains(text, "Y") {
			log.Fatalln("You must agree to mojang's EULA")
		}
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Thanks!")

	}
	err := os.Setenv("MCINSTALLER_FIRST_TIME", "FALSE")
	//fmt.Println(os.Getenv("MCINSTALLER_FIRST_TIME"))
	if err != nil {
		log.Fatal(err)
	}
	usr, _ := user.Current()
	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:    "update",
				Aliases: []string{"u", "up"},
				Usage:   "update a server to the latest/specified version",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "server",
						Aliases: []string{"s"},
						Usage:   "Select whether to install paper or spigot",
						Value:   "spigot",
					},
					&cli.StringFlag{
						Name:    "version",
						Aliases: []string{"v"},
						Usage:   "Select Minecraft version to install for",
						Value:   "1.15.2",
					},
					&cli.StringFlag{
						Name:     "output",
						Aliases:  []string{"o"},
						Usage:    "Path to install server jar to, should be empty",
						FilePath: usr.HomeDir + string(os.PathSeparator) + "mcserver",
					},
				},
				Action: func(c *cli.Context) error {
					path := c.String("output")
					server := c.String("server")
					verStr := c.String("version")
					/*if isTTY(){
						spinner := Spinner{
							message: "",
							i:       0,
						}
						go func() {
							for true{
								time.Sleep(200*time.Millisecond)
								spinner.Tick()
							}
						}()
					}*/
					if path == "" {
						log.Fatalln("Please specify the directory where you want your server to be with -o")
					}
					ver, err := version.NewVersion(verStr)
					if err != nil {
						return err
					}
					if server == "spigot" {
						spigot, err := installer.GetInstaller(installer.SpigotType)
						if err != nil {
							return err
							//log.Fatal("Error: Failed to parse server, we only currently support paper and spigot\n\n" + server)
						}
						if !spigot.SupportsVersion(*ver) {
							fmt.Println("Error: This software does not support " + verStr)
							time.Sleep(1 * time.Second)
							fmt.Println("We only support: ", installer.SupportedVersionStrings(spigot))
							return errors.New("unsupported minecraft version for server software")
						}
						err = spigot.Update(path, ver)
						if err != nil {
							return err
						}
					}
					if server == "paper" {
						paper, err := installer.GetInstaller(installer.PaperType)
						if err != nil {
							return err
							//log.Fatal("Error: Failed to parse server, we only currently support paper and spigot\n\n" + server)
						}
						if !paper.SupportsVersion(*ver) {
							fmt.Println("Error: This software does not support " + verStr)
							time.Sleep(1 * time.Second)
							fmt.Println("We only support: ", installer.SupportedVersionStrings(paper))
							return errors.New("unsupported minecraft version for server software")
						}
						err = paper.Update(path, ver)
						if err != nil {
							return err
						}

					}
					return nil
				},
			},
			{
				Name:    "install",
				Aliases: []string{"i", "in"},
				Usage:   "install a minecraft server to your specified directory",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "server",
						Aliases: []string{"s"},
						Usage:   "Select whether to install paper or spigot",
						Value:   "spigot",
					},
					&cli.StringFlag{
						Name:    "version",
						Aliases: []string{"v"},
						Usage:   "Select Minecraft version to install for",
						Value:   "1.15.2",
					},
					&cli.StringFlag{
						Name:     "output",
						Aliases:  []string{"o"},
						Usage:    "Path to install server jar to, should be empty",
						FilePath: usr.HomeDir + string(os.PathSeparator) + "mcserver",
					},
				},
				Action: func(c *cli.Context) error {
					path := c.String("output")
					server := c.String("server")
					verStr := c.String("version")

					if path == "" {
						log.Fatalln("Please specify the directory where you want your server to be with -o")
					}
					ver, err := version.NewVersion(verStr)
					if err != nil {
						return err
					}
					if server == "spigot" {
						spigot, err := installer.GetInstaller(installer.SpigotType)
						if err != nil {
							return err
							//log.Fatal("Error: Failed to parse server, we only currently support paper and spigot\n\n" + server)
						}
						if !spigot.SupportsVersion(*ver) {
							fmt.Println("Error: This software does not support " + verStr)
							time.Sleep(1 * time.Second)
							fmt.Println("We only support: ", installer.SupportedVersionStrings(spigot))
							return errors.New("unsupported minecraft version for server software")
						}
						err = spigot.Install(path, ver)
						if err != nil {
							return err
						}
					}
					if server == "paper" {
						paper, err := installer.GetInstaller(installer.PaperType)
						if err != nil {
							return err
							//log.Fatal("Error: Failed to parse server, we only currently support paper and spigot\n\n" + server)
						}
						if !paper.SupportsVersion(*ver) {
							fmt.Println("Error: This software does not support " + verStr)
							time.Sleep(1 * time.Second)
							fmt.Println("We only support: ", installer.SupportedVersionStrings(paper))
							return errors.New("unsupported minecraft version for server software")
						}
						err = paper.Install(path, ver)
						if err != nil {
							return err
						}

					}
					return nil
				},
			},
		},

		Name:  "mcinstall",
		Usage: "Download different minecraft server software",
	}

	err = app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
	/*flag.Usage = func() {
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), usage, os.Args[0])
		flag.PrintDefaults()
		return
	}
	flag.Parse()
	if flag.NArg() != 3 {
		log.Fatal("error: wrong number of arguments")
	}
	path := flag.Arg(0)
	rev := flag.Arg(2)
	var revVer version.Version
	if strings.HasSuffix(path, "/") || strings.HasSuffix(path, "\\") {
		if flag.Arg(1) == "spigot" {
			spigot, err := installer.GetInstaller(installer.SpigotType)
			if spigot.SupportsVersion(rev){

			}
			err = spigot.Install(path,)
		} else if flag.Arg(1) == "paper" {
			installer.InstallPaper(path, rev)
		}
	} else {
		path = path + string(os.PathSeparator)
		if flag.Arg(1) == "spigot" {
			installer.InstallSpigot(path, rev)
		} else if flag.Arg(1) == "paper" {
			installer.InstallPaper(path, rev)
		}
	}*/

}
func getProgramVersion() (*version.Version, error) {
	return version.NewVersion("1.1")
}
