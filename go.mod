module github.com/henry0w/mcinstall

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/hashicorp/go-version v1.2.0
	github.com/pkg/errors v0.8.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	github.com/urfave/cli/v2 v2.1.1
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
)
