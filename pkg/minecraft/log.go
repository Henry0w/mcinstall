package minecraft
import (
	"github.com/henry0w/mcinstall/pkg/logging"
)

var logger logging.Logger

func init() {
	logger = logging.DefaultLogger()
}

func SetLogger(l logging.Logger) {
	logger = l
}
