package logging

import (
	"go.uber.org/zap"
	"log"
)

type Logger interface {
	Debug(args ...interface{})
	Info(args ...interface{})
	Warn(args ...interface{})
	Error(args ...interface{})
	DPanic(args ...interface{})
	Panic(args ...interface{})
	Fatal(args ...interface{})
	Debugf(template string, args ...interface{})
	Infof(template string, args ...interface{})
	Warnf(template string, args ...interface{})
	Errorf(template string, args ...interface{})
	DPanicf(template string, args ...interface{})
	Panicf(template string, args ...interface{})
	Fatalf(template string, args ...interface{})
	Debugw(msg string, keysAndValues ...interface{})
	Infow(msg string, keysAndValues ...interface{})
	Warnw(msg string, keysAndValues ...interface{})
	Errorw(msg string, keysAndValues ...interface{})
	DPanicw(msg string, keysAndValues ...interface{})
	Panicw(msg string, keysAndValues ...interface{})
	Fatalw(msg string, keysAndValues ...interface{})
	Sync() error
}

var sharedLogger Logger

func init() {
	logger, err := NewLogger()
	sharedLogger = logger
	if err != nil {
		log.Fatal(err)
	}
}

func NewLogger() (Logger, error) {
	l, err := zap.NewDevelopment()
	if err != nil {
		return nil, err
	}
	defer l.Sync()
	return l.Sugar(), nil
}

func DefaultLogger() Logger {
	return sharedLogger
}
