package installer

import (
	"errors"
	"fmt"
	"github.com/hashicorp/go-version"
	"github.com/henry0w/mcinstall/pkg/path"
	"log"
	"os"
	"sort"
)

type paper struct {
	urlMap   map[string]string
	versions []*version.Version
}

const (
	paper18URL = "https://papermc.io/ci/job/Paper/443/artifact/paperclip.jar"
	paper1122  = "https://papermc.io/ci/job/Paper/lastSuccessfulBuild/artifact/paperclip.jar"
	paper1144  = "https://papermc.io/ci/job/Paper-1.14/lastSuccessfulBuild/artifact/paperclip.jar"
	paper1152  = "https://papermc.io/ci/job/Paper-1.15/lastSuccessfulBuild/artifact/paperclip.jar"
)

var paperVersionURLs = map[string]string{
	"1.8":    paper18URL,
	"1.8.8":  paper18URL,
	"1.8.9":  paper18URL,
	"1.12":   paper1122,
	"1.12.2": paper1122,
	"1.14":   paper1144,
	"1.14.4": paper1144,
	"1.15":   paper1152,
	"1.15.2": paper1152,
}

func newPaper() (*paper, error) {
	urlMap := make(map[string]string)
	var versions []*version.Version
	for v, url := range paperVersionURLs {
		ver, err := version.NewVersion(v)
		if err != nil {
			log.Fatalf("Bad version string %s: %v", v, err)
		}
		versions = append(versions, ver)
		urlMap[ver.String()] = url
	}
	sort.Sort(version.Collection(versions))
	return &paper{versions: versions,
		urlMap: urlMap}, nil
}

func (p *paper) ServerType() ServerType {
	return PaperType
}
func (p *paper) Name() string {
	return p.ServerType().String()
}

func (p *paper) SupportsVersion(v version.Version) bool {
	for _, sv := range p.versions {
		if sv.Equal(&v) {
			return true
		}
	}
	return false
}

func (p *paper) SupportedVersions() []*version.Version {
	return p.versions
}
func (p *paper) Update(destDir string, v *version.Version) error {
	logger.Info("Deleting all jars in path " + destDir)
	err := deleteAllJarsInPath(destDir)
	if err != nil {
		logger.Fatal(err)
		return err
	}
	logger.Info("Downloading paper jar...")
	jarPath := path.Join(destDir, "paper.jar")
	url, ok := p.urlMap[v.String()]
	if !ok {
		err = errors.New(fmt.Sprintf("No source JAR URL defined for version:  %s", v))
		logger.Error(err)
		return err
	}
	err = urlCopy(jarPath, url)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Done!")
	return nil
}

func (p *paper) Install(dest string, v *version.Version) error {
	if !p.SupportsVersion(*v) {
		return errors.New(fmt.Sprintf("Unsupported version: %s", v.String()))
	}
	var err error
	if !path.IsDir(dest) {
		err = errors.New(fmt.Sprintf("Invalid destination, not a directory: %s", dest))
		logger.Error(err)
		return err
	}
	jarPath := path.Join(dest, "paper.jar")
	url, ok := p.urlMap[v.String()]
	if !ok {
		err = errors.New(fmt.Sprintf("No source JAR URL defined for version:  %s", v))
		logger.Error(err)
		return err
	}
	err = urlCopy(jarPath, url)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Running Server to prepare eula...")
	err = runJar(dest+string(os.PathSeparator)+"paper.jar", dest)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Modifying eula...")
	err = modifyEULA(dest)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Done!")
	return nil
}
