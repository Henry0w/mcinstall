package installer

import (
	"github.com/hashicorp/go-version"
	"github.com/henry0w/mcinstall/pkg/logging"
	"log"
)

var serverTypeInstallerMap map[ServerType]Installer
var validServerTypes []ServerType
var installers []Installer

func init() {
	var err error
	serverTypeInstallerMap, err = newServerTypeInstallerMap()
	if err != nil {
		log.Fatal(err)
	}
	for t, i := range serverTypeInstallerMap {
		validServerTypes = append(validServerTypes, t)
		installers = append(installers, i)
	}
}

func ServerTypes() []ServerType {
	return validServerTypes
}

func Installers() []Installer {
	return installers
}
func newServerTypeInstallerMap() (map[ServerType]Installer, error) {
	installers := make(map[ServerType]Installer)
	paper, err := newPaper()
	if err != nil {
		return nil, err
	}
	installers[paper.ServerType()] = paper
	spigot, err := newSpigot()
	if err != nil {
		return nil, err
	}
	installers[spigot.ServerType()] = spigot
	return installers, err
}

type Installer interface {
	Install(destDir string, v *version.Version) error
	Update(destDir string, v *version.Version) error
	SupportsVersion(v version.Version) bool
	SupportedVersions() []*version.Version
	ServerType() ServerType
}

func GetInstaller(t ServerType) (Installer, error) {
	installerMap, err := newServerTypeInstallerMap()
	if err != nil {
		return nil, err
	}
	i, _ := installerMap[t]
	//fmt.Println(ok)
	/*if !ok {
		return nil, errors.New(fmt.Sprintf("Unsupported spoftware: %s", t.String()))
	}*/
	return i, nil
}

func SetLogger(l logging.Logger) {
	logger = l
}

func SupportedVersionStrings(i Installer) []string {
	versions := i.SupportedVersions()
	strs := make([]string, len(versions))
	for i, v := range versions {
		strs[i] = v.String()
	}
	return strs

}
