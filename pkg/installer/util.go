package installer

import (
	"bufio"
	"fmt"
	"github.com/henry0w/mcinstall/pkg/path"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

func runJar(jarPath string, workDir string) error {
	cmd := exec.Command(javaCommand(), "-jar", jarPath)
	cmd.Dir = workDir
	output, err := cmd.StdoutPipe()
	if err != nil {
		logger.Error(err)
		return err
	}
	err = cmd.Start()
	if err != nil {
		logger.Error(err)
		return err
	}
	readAndPrint(output)
	err = cmd.Wait()
	if err != nil {
		logger.Error(err)
		return err
	}
	return err
}
func readAndPrint(pipe io.Reader) {
	scanner := bufio.NewScanner(pipe)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		m := scanner.Text()
		logger.Info(m)
	}
}
func deleteAllJarsInPath(dirPath string) error {
	//var files []string
	dirInfo, err := ioutil.ReadDir(dirPath)
	if err != nil {
		panic(err)
	}
	var filesString string
	for _, file := range dirInfo {
		if strings.Contains(file.Name(), ".jar") {
			filesString += ", " + file.Name()
		}

	}
	logger.Info("This program will delete these files: " + filesString)
	fmt.Printf("\nDo you wish to continue [y/n]")
	reader := bufio.NewReader(os.Stdin)
	text, err := reader.ReadString('\n')
	if text == "n" || text == "N" {
		logger.Info("Exiting program...")
		time.Sleep(1 * time.Second)
		os.Exit(1)
	}
	for _, file := range dirInfo {
		if strings.HasSuffix(file.Name(), ".jar") {
			logger.Info("Deleting " + file.Name())
			err := os.Remove(filepath.Join(dirPath, file.Name()))
			if err != nil {
				logger.Fatal(err)
				return err
			}
		}
	}
	return nil
}

func modifyEULA(p string) error {
	if path.IsDir(p) {
		p = path.Join(p, "eula.txt")
	}
	eula, err := ioutil.ReadFile(p)
	if err != nil {
		return err
	}
	eulaString := string(eula)
	updatedEula := strings.Replace(eulaString, "false", "false", -1)
	err = ioutil.WriteFile(p, []byte(updatedEula), 0400)
	return err
}

func javaCommand() string {
	return "java"
}
