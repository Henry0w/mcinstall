package installer

import (
	"errors"
	"fmt"
	"github.com/hashicorp/go-version"
	"github.com/henry0w/mcinstall/pkg/path"
	"io"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

const (
	spigotBuildToolsJarURL = "https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar"
)

var spigotVersionStrings = []string{
	"1.15.2",
	"1.15.1",
	"1.15",
	"1.14.4",
	"1.14.3",
	"1.14.2",
	"1.14.1",
	"1.14",
	"1.13.2",
	"1.13.1",
	"1.13",
	"1.12.2",
	"1.12.1",
	"1.12",
	"1.11",
	"1.10",
	"1.9.4",
	"1.9.2",
	"1.9",
	"1.8.8",
	"1.8.7",
	"1.8.3",
	"1.8",
}

type spigot struct {
	versions []*version.Version
}

func newSpigot() (*spigot, error) {
	versions := make([]*version.Version, len(spigotVersionStrings))
	for i, v := range spigotVersionStrings {
		ver, err := version.NewVersion(v)
		if err != nil {
			return nil, err
		}
		versions[i] = ver
	}
	return &spigot{versions: versions}, nil
}

/*var startupScript = `#!/bin/sh
java -jar %JAVAARGS% %JARFILE%`*/

func (s *spigot) ServerType() ServerType {
	return SpigotType
}

func (s *spigot) Name() string {
	return s.ServerType().String()
}

func (s *spigot) SupportedVersions() []*version.Version {
	return s.versions
}
func (s *spigot) Update(destDir string, v *version.Version) error {
	logger.Info("Deleting all jars in path " + destDir)
	err := deleteAllJarsInPath(destDir)
	if err != nil {
		logger.Fatal(err)
		return err
	}
	logger.Info("Downloading BuildTools...")
	jarPath, err := s.downloadBuildToolsJar(destDir)
	if err != nil {
		logger.Fatal(err)
		return err
	}
	logger.Info("Running BuildTools...")
	err = s.runBuildToolsJar(destDir, jarPath, v)
	if err != nil {
		logger.Fatal(err)
		return err
	}
	logger.Info("Done!")

	return nil
}
func (s *spigot) SupportsVersion(v version.Version) bool {
	for _, sv := range s.SupportedVersions() {
		if sv.Equal(&v) {
			return true
		}
	}
	return false
}

func (s *spigot) Install(destDir string, v *version.Version) error {
	logger.Info("Downloading installer (BuildTools) to ", destDir, "...")
	var err error
	if !path.IsDir(destDir) {
		err = errors.New(fmt.Sprintf("Invalid destination, not a directory: %s", destDir))
		logger.Error(err)
		return err
	}
	logger.Info("Installing build tools jar..")
	jarPath, err := s.downloadBuildToolsJar(destDir)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Installing server using BuildTools...")
	err = s.runBuildToolsJar(destDir, jarPath, v)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Running Server to prepare eula...")
	spigotJar, err := s.spigotJar(destDir, v)
	if err != nil {
		logger.Error(err)
		return err
	}
	err = runJar(spigotJar, destDir)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Modifying eula...")
	err = modifyEULA(destDir)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Done!")
	return nil
}

func (s *spigot) spigotJar(destDir string, v *version.Version) (string, error) {
	p := path.Join(destDir, fmt.Sprintf("spigot-%s.jar", v.String()))
	_, err := path.Stat(p)
	if err != nil {
		return p, err
	}
	return p, nil
}

func (s *spigot) runBuildToolsJar(destDir string, jarPath string, version *version.Version) error {
	cmd := exec.Command(javaCommand(), "-jar", jarPath, "--rev", version.String())
	if !strings.HasSuffix(destDir, string(os.PathSeparator)) {
		destDir = destDir + string(os.PathSeparator)
	}
	cmd.Dir = destDir
	output, err := cmd.StdoutPipe()
	if err != nil {
		logger.Error(err)
		return err
	}
	err = cmd.Start()
	if err != nil {
		logger.Error(err)
		return err
	}

	readAndPrint(output)
	err = cmd.Wait()
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (s *spigot) downloadBuildToolsJar(destDir string) (string, error) {
	jarPath := path.Join(destDir, "BuildTools.jar")
	err := urlCopy(jarPath, spigotBuildToolsJarURL)
	if err != nil {
		logger.Errorf("Error: Failed to download spigot (BuildTools): %s", err)
		return "", err
	}
	return jarPath, err
}

func urlCopy(dest string, url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}
