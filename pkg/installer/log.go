package installer

import (
	"bufio"
	"bytes"
	"github.com/henry0w/mcinstall/pkg/logging"
)

var logger logging.Logger

func init() {
	logger = logging.DefaultLogger()
}

func logOutput(output []byte) {
	scanner := bufio.NewScanner(bytes.NewReader(output))
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		m := scanner.Text()
		logger.Info(m)
	}
}