package installer
import (
	"errors"
	"fmt"
)
type ServerType int
const (
	PaperType = iota
	SpigotType
)

var serverTypes = []ServerType{PaperType, SpigotType,}
var serverTypeNames = []string{"paper", "spigot",}


func (s ServerType)String() string {
	return serverTypeNames[s]
}

func GetServerType(s string) (*ServerType, error) {
	for _, t := range serverTypes {
		if t.String() == s {
			return &t, nil
		}
	}
	return nil, errors.New(fmt.Sprintf("Unknown server type: '%s'", s))
}

