package path

import (
	"fmt"
	"os"
	"strings"
)

func Open(path string) (*os.File, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	return file, nil
}

func Stat(path string) (os.FileInfo, error) {
	file, err := Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	fi, err := file.Stat()
	if err != nil {
		return nil, err
	}
	return fi, nil
}

func IsDir(p string) bool {
	//TODO: fix
	return true
}

func Join(dir string, name string) string {
	sep := string(os.PathSeparator)
	if strings.HasSuffix(dir, sep) || strings.HasPrefix(name, sep) {
		sep = ""
	}
	return fmt.Sprintf("%s%s%s", dir, sep, name)
}
